package sec.project.controller;

import sec.project.repository.DatabaseQueries;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sec.project.domain.Signup;

@Controller
public class SignupController {

    @RequestMapping("*")
    public String defaultMapping(Model model, HttpSession httpSession) {
        return "redirect:/form";
    }
    
    @RequestMapping(value = "/hidden", method = RequestMethod.GET)
    public String redirectURL(@RequestParam String url) throws SQLException 
    {
        return "redirect:" + url;
    }
    
    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String loadForm() {
        return "form";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@RequestParam String username, @RequestParam String pass) throws SQLException 
    {
        DatabaseQueries db = new DatabaseQueries();
        Signup login;
        login = db.getAccount(username, pass);
        if (login != null) 
        {
            return "hidden";
        }

        return null;
    }

}
