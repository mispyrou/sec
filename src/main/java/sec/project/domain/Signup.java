package sec.project.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.stereotype.Controller;

@Controller
public class Signup {

    @Id
    private int id;
    private String username;
    private String pass;

    public Signup() {
        super();
    }

    public Signup(int id, String username, String pass) {
        this.id = id;
        this.username = username;
        this.pass = pass;
    }
    
    public int getId() 
    {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return username;
    }

    public void setName(String name) {
        this.username = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String address) {
        this.pass = pass;
    }

}
